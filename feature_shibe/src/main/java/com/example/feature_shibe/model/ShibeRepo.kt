package com.example.feature_shibe.model

import android.content.Context
import com.rave.shibe.model.local.ShibeDatabase
import com.example.feature_shibe.model.local.entity.Shibe
import com.rave.shibe.model.remote.ShibeService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

private const val TAG = "ShibeRepo"
class ShibeRepo(context: Context) {

    private val shibeService = ShibeService.getInstance()
    val shibeDao = ShibeDatabase.getInstance(context).shibeDao()
    suspend fun getShibes() = withContext(Dispatchers.IO) {
        val cachedShibes: List<Shibe> = shibeDao.getAll()

        return@withContext cachedShibes.ifEmpty {
            val shibeUrls: List<String> = shibeService.getShibes()
            val shibes: List<Shibe> = shibeUrls.map { Shibe(url = it) }
            shibeDao.insert(shibes)
            return@ifEmpty shibes
        }
    }
}