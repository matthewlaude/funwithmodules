package com.example.bottomsup.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bottomsup.model.BottomsUpRepo
import com.example.bottomsup.model.response.CategoryDTO
import com.example.bottomsup.view.category.CategoryState
import kotlinx.coroutines.launch

class CategoryViewModel : ViewModel() {
    private val repo by lazy { BottomsUpRepo }

    private val _state = MutableLiveData(CategoryState())
    val state: LiveData<CategoryState> get() = _state

    private val _stringList = MutableLiveData<CategoryDTO>()
    val stringList: LiveData<CategoryDTO> get() = _stringList

//    init {
//        viewModelScope.launch {
//            val categoryDTO = repo.getCategories()
//            _state.value = CategoryState(categories = categoryDTO.categoryItems)
//            Log.d("CategoryViewModel", categoryDTO.toString())
//        }
//    }

    fun getCategories() {
        viewModelScope.launch {
            val category = repo.getCategories()
            _stringList.value = category
        }
    }
}