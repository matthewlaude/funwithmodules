package com.example.bottomsup.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bottomsup.model.BottomsUpRepo
import com.example.bottomsup.model.response.DrinkDetailsDTO
import com.example.bottomsup.model.response.DrinkListDTO
import com.example.bottomsup.view.details.DetailState
import com.example.bottomsup.view.drink.DrinkListState
import kotlinx.coroutines.launch

class DetailViewModel : ViewModel() {
    private val repo by lazy { BottomsUpRepo }

    private val _state = MutableLiveData(DetailState())
    val state: LiveData<DetailState> get() = _state

    private val _stateList = MutableLiveData<DrinkDetailsDTO>()
    val stateList: LiveData<DrinkDetailsDTO> get() = _stateList

    fun getDetails(drinkId: String) {
        viewModelScope.launch {
            val drinkDetailsDTO = repo.getDrinkDetails(drinkId)
            _state.value = DetailState(details = drinkDetailsDTO.drinks)
            _stateList.value = drinkDetailsDTO
        }
    }
}