package com.example.feature_bottoms.view.drink

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bottomsup.adapter.DrinkListAdapter
import com.example.bottomsup.viewmodel.DrinkListViewModel
import com.example.feature_bottoms.databinding.FragmentDrinkListBinding

class DrinkListFragment : Fragment() {

//    companion object {
//        private const val TAG = "DrinkListFragment"
//    }

    private var _binding: FragmentDrinkListBinding? = null
    private val binding get() = _binding!!

    //    private val args by navArgs<DrinkListFragmentArgs>()
    private val drinkListViewModel by viewModels<DrinkListViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDrinkListBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val category = arguments?.getString("category")
//        binding.tvDrinks.text = category.toString()

        with(binding.rvDrinks) {
            layoutManager = LinearLayoutManager(this.context)
            adapter = DrinkListAdapter().apply {
                with(drinkListViewModel) {
                    getDrink(category!!)
                    stateList.observe(viewLifecycleOwner) {
                        addDrinks(it.drinks)
                    }
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}