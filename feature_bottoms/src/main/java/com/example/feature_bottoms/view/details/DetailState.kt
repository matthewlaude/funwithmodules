package com.example.bottomsup.view.details

import com.example.bottomsup.model.response.DrinkDetailsDTO

data class DetailState(
    val details: List<DrinkDetailsDTO.Drink> = emptyList()
)