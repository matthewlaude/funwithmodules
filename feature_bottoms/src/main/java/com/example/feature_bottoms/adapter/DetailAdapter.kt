package com.example.bottomsup.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.bottomsup.model.response.DrinkDetailsDTO
import com.example.feature_bottoms.databinding.ItemDetailsBinding
import com.squareup.picasso.Picasso

class DetailAdapter : RecyclerView.Adapter<DetailAdapter.DetailsViewHolder>() {

    private var details = listOf<DrinkDetailsDTO.Drink>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailsViewHolder {
        return DetailsViewHolder.getInstance(parent)
    }

    override fun onBindViewHolder(holder: DetailsViewHolder, position: Int) {
        holder.bindDetail(details[position])
    }

    override fun getItemCount(): Int {
        return details.size
    }

    fun addDetails(details: List<DrinkDetailsDTO.Drink>) {
        this.details = details
        notifyDataSetChanged()
    }

    class DetailsViewHolder(
        private val binding: ItemDetailsBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bindDetail(detail: DrinkDetailsDTO.Drink) = with(binding) {
            tvItemDetail1.text = detail.strDrink
            tvItemDetail2.text = detail.strInstructions
            Picasso.get().load(detail.strDrinkThumb).into(ivImageDetail)
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemDetailsBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { DetailsViewHolder(it) }
        }
    }
}