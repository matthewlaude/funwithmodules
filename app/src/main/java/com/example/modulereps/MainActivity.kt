package com.example.modulereps

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.navigation.NavHost
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.MyColor
import com.example.modulereps.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
//    companion object {
//        private const val TAG = "MainActivity"
//    }

    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    private val navHostFragment by lazy {
        supportFragmentManager.findFragmentById(R.id.fragment_host) as NavHostFragment
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
//        Log.d(TAG, "My random color is ${MyColor.randomColor}")
        binding.bottomNavigation.setupWithNavController(navHostFragment.navController)
//        binding.
    }
}